//Import thư viện expressjs tương đương import express from "express";
const express = require("express");

//Khởi tạo 1 app express
const app = express();

// khai báo cổng chạy project
const port = 8000;

// Callback function là 1 function đóng vai trò là tham số của 1 func khác, nó sẽ dc thực hiện khi func chủ dc gọi
// Khai báo API dạng /
app.get("/", (req, res) => {
  let today = new Date();

  res.json({
    message: `Xin chào, hôm nay là ngày ${today.getDate()} tháng ${
      today.getMonth() + 1
    } năm ${today.getFullYear()}`,
  });
});

// Khai báo API dạng GET
app.get("/get-method", (req, res) => {
  res.json({
    message: "GET method",
  });
});

// Khai báo dạng POST
app.post("/post-method", (req, res) => {
  res.json({
    message: "POST method",
  });
});

// Khai báo dạng PUT
app.put("/put-method", (req, res) => {
  res.json({
    message: "PUT method",
  });
});

// Khai báo dạng DELETE
app.delete("/delete-method", (req, res) => {
  res.json({
    message: "DELETE method",
  });
});

app.listen(port, () => {
  console.log("App listening on port: ", port);
});
